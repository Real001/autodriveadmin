var query = require('./connection');

var orders = function () {

    this.getAllOrders = function (callback) {
        query('SELECT * FROM orders', null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.addOrders = function (orders, callback) {
        query('INSERT INTO orders ( numberOrder, idClient, idEmployee, orderPrice, costSpareParts, total, status, dateReceipt, timeReceipt, dateBegin, endDate, timeBegin, endTime) ' +
            'VALUES ( :numberOrder, :idClient, :idEmployee, :orderPrice, :costSpareParts, :total, :status, :dateReceipt, :timeReceipt, :dateBegin, :endDate, :timeBegin, :endTime)',
            {numberOrder: orders.numberOrder, idClient: orders.idClient, idEmployee: orders.idEmployee, orderPrice: orders.orderPrice, costSpareParts: orders.costSpareParts, total: orders.total,
            status: orders.status, dateReceipt: orders.dateReceipt, timeReceipt: orders.timeReceipt, dateBegin: orders.dateBegin, endDate: orders.endDate, timeBegin: orders.timeBegin, endTime: orders.endTime},
            function (err, rows) {
                orders.id = rows.insertId;
                if (!err){
                    callback(null);
                } else {
                    callback(err);
                }
            });
    }

    this.getAllOrdersClient = function (id, callback) {
        query('SELECT orders.*, employee.login FROM orders, employee WHERE orders.idClient = :id and employee.id = orders.idEmployee', {id: id},function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.getOrder = function (numberOrder, callback) {
        query('SELECT * FROM orders WHERE orders.numberOrder = :numberOrder',{numberOrder:numberOrder}, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.updateOrder = function (order, callback) {
        query('UPDATE orders SET orderPrice = :orderPrice, costSpareParts = :costSpareParts, total = :total, status = :status, dateBegin = :dateBegin,' +
            'timeBegin = :timeBegin, endTime = :endTime, endDate = :endDate WHERE numberOrder = :numberOrder', {numberOrder: order.numberOrder,
        orderPrice: order.orderPrice, costSpareParts: order.costSpareParts, total: order.total, status: order.status, dateBegin: order.dateBegin,
        timeBegin: order.timeBegin, endTime: order.endTime, endDate: order.endDate}, function (err, rows) {
            if (!err){
                callback(null);
            } else {
                callback(err);
            }
        });
    }

};

module.exports = {
    orders: new orders()
};
var query = require('./connection');

var spareParts = function () {

    //получение данных из бд о запчасти по её id
    this.getSpareParts = function (id, callback) {
        query('SELECT * FROM spareparts WHERE id = :id', {id: id}, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows[0]);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    //получение всего списка доступных запчастей из БД
    this.getAllSpareParts = function (callback) {
        query('SELECT * FROM spareparts', null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    //добавление новой запчасти в БДs
    this.addSpareParts = function (spareParts, callback) {
        query('INSERT INTO spareparts (name, vendorCode, manufacturer, manufacturerCountry, application, OEM, price, sellingPrice, unit, notes) ' +
            'VALUES (:name, :vendorCode, :manufacturer, :manufacturerCountry, :application, :OEM, :price, :sellingPrice, :unit, :notes)',
            {name: spareParts.name, vendorCode: spareParts.vendorCode, manufacturer: spareParts.manufacturer, manufacturerCountry: spareParts.manufacturerCountry,
                application: spareParts.application, OEM: spareParts.OEM, price: spareParts.price, sellingPrice: spareParts.sellingPrice, unit: spareParts.unit, notes: spareParts.notes},
            function (err, rows) {
                spareParts.id = rows.insertId;
                if (!err) {
                    callback(null);
                } else {
                    callback(err);
                }
            });
    }

}

module.exports = {
    spareParts: new spareParts()
}
var query = require('./connection');

var works = function () {

    this.addAllWorks = function (works, callback) {
        query('INSERT INTO work (idServices, services, price, quantity, amount, dateBegin, endDate, status, notes, executor, responsible, idClient, codeOrder, idCar) ' +
            'VALUES ( :idServices, :services, :price, :quantity, :amount, :dateBegin, :endDate, :status, :notes, :executor, :responsible, :idClient, :codeOrder, :idCar)',
            {idServices: works.idServices, services: works.services, price: works.price, quantity: works.quantity, amount: works.amount, dateBegin: works.dateBegin,
                endDate: works.endDate, status: works.status, notes: works.notes, executor: works.executor, responsible: works.responsible, idClient: works.idClient,
            codeOrder: works.codeOrder, idCar: works.idCar}, function (err, rows) {
                works.id = rows.insertId;
            if (!err){
                    callback(null);
                } else {
                    callback(err);
                }
            });
    }

    //вывод списка работ заказа
    this.getAllWorksOrder = function (numberOrder, callback) {
        query('SELECT work.*, employee.login, services.name FROM work, employee, services WHERE work.codeOrder = :numberOrder and employee.id = work.executor and services.id = work.services ', {numberOrder: numberOrder}, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }


};

module.exports = {
    works: new works()
};
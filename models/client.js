var query = require('./connection');

var clients = function () {
    
    this.getClients = function (id, callback) {
        // query('SELECT clients.id, clients.fullName, clients.typeClient, clients.phone, clients.email, cars.type, cars.brand, cars.model, cars.year, cars.stateNumber, cars.VIN, cars.mileage  ' +
        //     'FROM clients, cars WHERE clients.id=cars.idClient AND clients.id = :id', {id: id}, function (err, rows) {
        query('SELECT * FROM clients INNER JOIN cars ON clients.id = cars.idClient WHERE clients.id = :id',{id:id}, function (err, rows) {

            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.getAllClients = function(callback) {
        query('SELECT *FROM clients', null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }
    
    this.addClients = function (clients, callback) {
       query('INSERT INTO clients (fullName, typeClient, phone, email) VALUES ( :fullName, :typeClient, :phone, :email)',
           {fullName: clients.fullName, typeClient: clients.typeClient, phone: clients.phone, email: clients.email},
       function (err, rows) {
           clients.id = rows.insertId;
           query('INSERT INTO cars (type, brand, model, year, stateNumber, VIN, mileage, idClient) VALUES ( :type, :brand, :model, :year, :stateNumber, :VIN, :mileage, :idClient)',
               {type: clients.type, brand: clients.brand, model: clients.model, year: clients.year, stateNumber: clients.stateNumber,
               VIN: clients.VIN, mileage: clients.mileage, idClient: rows.insertId}, function (err) {
                   if (!err){
                       callback(null);
                   } else {
                       callback(err);
                   }
               });
           if (!err){
               callback(null);
           } else {
               callback(err);
           }
       });
    }

    this.updateClient = function (client, callback) {
        query('UPDATE clients SET fullName = :fullName, typeClient = :typeClient, phone = :phone, email = :email  WHERE id = :id',
            {id :client.id, fullName: client.fullName, typeClient: client.typeClient, phone: client.phone, email: client.email}, function (err) {
                if (!err){
                    callback(null);
                } else {
                    callback(err);
                }
            });
    }
};

module.exports = {
    clients: new clients()
};
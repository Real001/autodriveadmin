var query = require('./connection');

var employee  = function () {
    
    this.getEmployee = function (id, callback) {
        query('SELECT * FROM employee WHERE id = :id', {id: id}, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows[0]);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.getAllEmployee = function(callback) {
        query('SELECT *FROM employee', null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }

    this.getAllEmployeeAutomechanic = function (callback) {
        query("SELECT * FROM employee WHERE post = 'Мастер'", null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else  {
                callback(err);
            }
        });
    }

    this.getAllEmployeeInspector = function (callback) {
        query("SELECT * FROM employee WHERE post = 'Приемщик'", null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else  {
                callback(err);
            }
        });
    }
    this.getAllEmployeeSeller = function (callback) {
        query("SELECT * FROM employee WHERE post = 'Продавец'", null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else  {
                callback(err);
            }
        });
    }
}

module.exports = {
    employee: new employee()
};
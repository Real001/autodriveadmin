var mysql = require('mysql');
var config = require('../config/database');
var pool = mysql.createPool(
    {host           : "localhost",
        user           : "root",
        password       : "1234",
        database       : "autodrive",
        connectionLimit: 10});

function query(query, parameters, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            console.log(err);
            if (typeof (callback) === "function")
                callback(err);
        } else {
            connection.config.queryFormat = function (query, values) {
                if (!values)
                    return query;
                return query.replace(/\:(\w+)/g, function (txt, key) {
                    if (values.hasOwnProperty(key)) {
                        return this.escape(values[key]);
                    }
                    return txt;
                }.bind(this));
            }
            connection.query(query, parameters, function (err, rows) {
                connection.release();
                if (err) {
                    console.log(err);
                    if (typeof (callback) === "function")
                        callback(err);
                } else {
                    if (typeof (callback) === "function") callback(null, rows);
                }
            });
        }
    });
}

module.exports = query;
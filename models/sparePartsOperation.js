var query = require('./connection');

var sparePartsOperation = function () {

    this.addSparePartsOperation = function (spareParts, callback) {
        query('INSERT INTO sparepartsoperation (idSpareParts, spareParts, unit, vendorCode, price, quantity, amount, idClient, codeOrder)' +
            'VALUES (:idSpareParts, :spareParts, :unit, :vendorCode, :price, :quantity, :amount, :idClient, :codeOrder)', {
            idSpareParts: spareParts.idSpareParts, spareParts: spareParts.spareParts, unit: spareParts.unit, vendorCode: spareParts.vendorCode,
            price: spareParts.price, quantity: spareParts.quantity, amount: spareParts.amount, idClient: spareParts.idClient, codeOrder: spareParts.codeOrder
        }, function (err, rows) {
            spareParts.id = rows.insertId;
            if (!err) {
                callback(null);
            } else {
                callback(err);
            }
        });
    }
};

module.exports = {
    sparePartsOperation: new sparePartsOperation()
};
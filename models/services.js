var query = require('./connection');

var services = function () {

    this.getServices = function (id, callback) {
        query('SELECT * FROM services WHERE id = :id', {id: id}, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows[0]);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }
    
    this.getAllServices = function (callback) {
        query('SELECT * FROM services', null, function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }
    
    this.addService = function (service, callback) {
        query('INSERT INTO services (name, category, price, relatedMaterials, notes, typeCar) VALUES (:name, :category, :price, :relatedMaterials, :notes, :typeCar)',
            {name: service.name, category: service.category, price: service.price, relatedMaterials: service.relatedMaterials, notes: service.notes, typeCar: service.typeCar},
            function (err, rows) {
                service.id = rows.insertId;
                if (!err) {
                    callback(null);
                } else {
                    callback(err);
                }
        });
    }

}

module.exports = {
    services: new services()
}
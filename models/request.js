var query = require('./connection');

var requestDatabase = function () {

    this.getRequestDatabase = function (requestText, callback) {
        console.log(requestText + " model")
        query(requestText,  function (err, rows) {
            if (!err) {
                if (rows[0]) {
                    callback(null, rows);
                } else {
                    callback(null, null);
                }
            } else {
                callback(err);
            }
        });
    }
}

module.exports = {
    requestDatabase : new requestDatabase()
}
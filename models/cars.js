var query = require('./connection');

var cars = function () {

    this.addCars = function (cars, callback) {
        query('INSERT INTO cars (type, brand, model, year, stateNumber, VIN, mileage, idClient) VALUES ( :type, :brand, :model, :year, :stateNumber, :VIN,' +
            ':mileage, :idClient)', {type: cars.type, brand: cars.brand, model: cars.model, year: cars.year, stateNumber: cars.stateNumber, VIN: cars.VIN, mileage: cars.mileage, idClient: cars.idClient},
            function (err, rows) {
                cars.id = rows.insertId;
                if (!err){
                    callback(null);
                } else {
                    callback(err);
                }
            });

    }
}

module.exports = {
    cars: new cars()
};
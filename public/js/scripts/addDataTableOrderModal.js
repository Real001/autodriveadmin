var jobs = new Array();
var sparePart = new Array();
var amountJob, amountSparePart;

function addJobOrder() {
    var form = $('#addJobOrder').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }
    //добавление в массив нового объекта с данными о работе добавленной в заказ
    jobs.push(result);
    //вывод данных о добавленной работе в таблицу нового заказа
    $('#tableJobOrder').append('<tr>' +
        '<td>'+jobs.length +'</td>' +
        '<td>'+result.serviceCodeJob+'</td>' +
        '<td>'+nameJob+'</td>' +
        '<td>'+result.priceServiceJob+'</td>' +
        '<td>'+result.quantityServiceJob+'</td>' +
        '<td>'+result.amountServiceJob+'</td>' +
        '<td>'+result.dateBeginJob+'</td>' +
        '<td>'+result.dateExpJob+'</td>' +
        '<td>'+result.statusJob+'</td>' +
        '<td>'+result.executorJob+'</td>' +
        '<td>'+result.responsibleJob+'</td>' +
        '<td>'+result.notesJob+'</td>' +
        '<td>'+result.codeOrderJob+'</td>' +
        '<td>'+result.codeClientJob+'</td>' +
        '</tr>');

    //вычесление стоимости всех запчастей в новом заказе
    amountJob = 0;
    for (var i = 0; i <jobs.length; i++) {
        amountJob += +jobs[i].amountServiceJob;
    }
    $('#orderPrice').val(amountJob);
    $("#total").val(amountSparePart+amountJob);
}

function addSparePartOrder() {

    var form = $('#addSparePartOrder').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    //добавление в массив нового объекта с данными о запчасти добавленной в заказ
    sparePart.push(result);
    //вывод данных о запчсти в таблицу нового заказа
    $('#tableSparePartOrder').append('<tr>' +
        '<td>'+ sparePart.length +'</td>' +
        '<td>'+result.codeSparePart+'</td>' +
        '<td>'+nameSparePart+'</td>' +
        '<td>'+result.unitSparePart+'</td>' +
        '<td>'+result.vendorCodeSparePart+'</td>' +
        '<td>'+result.priceSparePart+'</td>' +
        '<td>'+result.numberSparePart+'</td>' +
        '<td>'+result.amountSparePart+'</td>' +
        '<td>'+result.codeClientSparePart+'</td>' +
        '<td>'+result.codeOrderSparePart+'</td>' +
        '</tr>');

    //вычисление всей суммы услуг используемых в новом заказе
    amountSparePart = 0;
    for (var i = 0; i <sparePart.length; i++) {
        amountSparePart += +sparePart[i].amountSparePart;
    }
    $('#costSpareParts').val(amountSparePart);
    $("#total").val(amountSparePart+amountJob);
}

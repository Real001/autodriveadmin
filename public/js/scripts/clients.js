function informClient(id) {

    if (!$("#fullNameCI").attr("readonly")) {
        $("#fullNameCI").attr("readonly", true);
        $("#typeClientCI").attr("readonly", true);
        $("#phoneCI").attr("readonly", true);
        $("#emailCI").attr("readonly", true);
        $("#saveClientButton").attr("disabled", true);
        $("#addCarButton").attr("disabled", true);
    }

    $.get('/client/id/' + id, function (data) {
        data = JSON.parse(data);
        $("#idCI").val(data[0].idClient);
        $("#fullNameCI").val(data[0].fullName);
        $("#typeClientCI").val(data[0].typeClient);
        $("#phoneCI").val(data[0].phone);
        $("#emailCI").val(data[0].email);
        $('#tableCarClient tbody').remove();
        //вывод списка авто клиента
        for (var i = 0; i < data.length; i++) {
            $('#tableCarClient').append('<tr>' +
                '<td>' + (i + 1) + '</td>' +
                '<td>' + data[i].brand + '</td>' +
                '<td>' + data[i].model + '</td>' +
                '<td>' + data[i].type + '</td>' +
                '<td>' + data[i].year + '</td>' +
                '<td>' + data[i].VIN + '</td>' +
                '<td>' + data[i].stateNumber + '</td>' +
                '<td>' + data[i].mileage + '</td>' +
                '</tr>');
        }
    });
    $("#clientInformation").modal();

}

function delatr() {
    if ($("#fullNameCI").attr("readonly")) {
        $("#fullNameCI").removeAttr("readonly");
        $("#typeClientCI").removeAttr("readonly");
        $("#phoneCI").removeAttr("readonly");
        $("#emailCI").removeAttr("readonly");
        $("#saveClientButton").removeAttr("disabled");
        $("#addCarButton").removeAttr("disabled");
    }
}

function addCar() {
    //получение данных формы addCar
    var form = $('#addCar').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }
    //id клиента для добавления авто
    result.idClient = $("#idCI").val();
    // отправка данных через ajax, для последующей их обработки
    sendJSON('/addCarClient', result, function (err, data) {
        if (!err && data.status == "success") {
            $('#tableCarClient').append('<tr>' +
                '<td>' + 100 + '</td>' +
                '<td>' + result.brandCar + '</td>' +
                '<td>' + result.modelCar + '</td>' +
                '<td>' + result.typeCar + '</td>' +
                '<td>' + result.yearCar + '</td>' +
                '<td>' + result.VINCar + '</td>' +
                '<td>' + result.stateNumberCar + '</td>' +
                '<td>' + result.mileageCar + '</td>' +
                '</tr>');
        }
    });
}

function updateClient() {
    var form = $('#informClientForm').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    sendJSON('/updateClient', result, function (err, data) {
        if (!err && data.status == "success") {
            window.location.href = "/clients";
        }
    });
}

function clientOrders(id) {
    $.get('/client/orders/' + id, function (data) {
        data = JSON.parse(data);
        $('#tableOrdersClient tbody').remove();
        for (var i = 0; i < data.length; i++) {
            var n = '"' + data[i].numberOrder + '"';
            $('#tableOrdersClient').append("<tr onclick='listServicesOrder(" + n + ");'>" +
                '<td>' + (i + 1) + '</td>' +
                '<td>' + data[i].numberOrder + '</td>' +
                '<td>' + data[i].dateReceipt + '</td>' +
                '<td>' + data[i].timeReceipt + '</td>' +
                '<td>' + data[i].orderPrice + '</td>' +
                '<td>' + data[i].costSpareParts + '</td>' +
                '<td>' + data[i].endDate + '</td>' +
                '<td>' + data[i].login + '</td>' +
                '<td>' + data[i].status + '</td>' +
                '</tr>');
        }
    });
}
//отображение работ заказа в таблице
function listServicesOrder(numberOrder) {
    workOrderList(numberOrder, function (data) {
        $("#worksOrderTable tbody").remove();
        for (var i = 0; i < data.length; i++) {
            $('#worksOrderTable').append("<tr>" +
                '<td>' + (i + 1) + '</td>' +
                '<td>' + data[i].idServices + '</td>' +
                '<td>' + data[i].name + '</td>' +
                '<td>' + data[i].quantity + '</td>' +
                '<td>' + data[i].price + '</td>' +
                '<td>' + data[i].amount + '</td>' +
                '<td>' + data[i].dateBegin + '</td>' +
                '<td>' + data[i].endDate + '</td>' +
                '<td>' + data[i].responsible + '</td>' +
                '<td>' + data[i].login + '</td>' +
                '<td>' + data[i].status + '</td>' +
                '<td>' + data[i].codeOrder + '</td>' +
                '</tr>');
        }
    });
}

//присвоение кода клиента в заказ и работу
function idclient(id) {
    $("#idClient").val(id.value);
    $("#codeClientJob").val(id.value);
}

function newOrder() {
    //создание номера заказа
    var random = Math.random() * (999999999999 - 1) + 1;
    var numberOrder = "АД" + random;
    numberOrder = numberOrder.toString().replace(".", "-");
    $("#numberOrder").val(numberOrder);
    $("#codeOrderJob").val(numberOrder);

    //получение даты и времени при создании нового заказа
    // var date = new Date();
    // var format = new Intl.DateTimeFormat("ru");
    // date = format.format(date);
    var date = dateNormal.dateFormat();
    $("#dateReceipt").val(date);
    $("#dateBeginJob").val(date);


    var time = timeNormal.timeFormat();
    $("#timeReceipt").val(time);

    $(".chosen-select").chosen();
}

function newWork() {
    //получение списка услуг
    servicesList(function (data) {
        $("#serviceJob option").remove();
        $("#serviceJob").append($('<option value="" disabled></option>'));
        for (var i = 0; i < data.length; i++) {
            $("#serviceJob").append($('<option value="' + data[i].id + '">' + data[i].name + '</option>'));
        }
    });

    //получение списка механиков и их вывод
    mechanicList(function (data) {
        $("#executorJob option").remove();
        $("#executorJob").append($('<option value="" disabled></option>'));
        for (var i = 0; i < data.length; i++) {
            $("#executorJob").append($('<option value="' + data[i].id + '">' + data[i].login + '</option>'));
        }
    });
}

//отправка ajax-запроса на получение данных об услуге вывод полученных значений
function ajaxServiceId(id) {
    $.get('/services/id/' + id, function (data) {
        data = JSON.parse(data);
        $("#serviceCodeJob").val(data.id);
        $("#priceServiceJob").val(data.price);

        nameJob = data.name;
    });
}

var jobs = new Array();//массив добавленных услуг(работ)
var amountJob;//стоимость всех добавленных услуг
var sparePart = null;

//добавление услуги
function addJobOrder() {
    var form = $('#addJobOrder').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }
    //добавление в массив нового объекта с данными о работе добавленной в заказ
    jobs.push(result);
    //вывод данных о добавленной работе в таблицу нового заказа
    $('#tableJobOrder').append('<tr>' +
        '<td>' + jobs.length + '</td>' +
        '<td>' + result.serviceCodeJob + '</td>' +
        '<td>' + nameJob + '</td>' +
        '<td>' + result.quantityServiceJob + '</td>' +
        '<td>' + result.priceServiceJob + '</td>' +
        '<td>' + result.amountServiceJob + '</td>' +
        '<td>' + result.dateBeginJob + '</td>' +
        '<td>' + result.dateExpJob + '</td>' +
        '<td>' + result.responsibleJob + '</td>' +
        '<td>' + result.executorJob + '</td>' +
        '<td>' + result.statusJob + '</td>' +
        '<td>' + result.codeOrderJob + '</td>' +
        '<td>' + result.codeClientJob + '</td>' +
        '</tr>');

    // вычесление стоимости всех запчастей в новом заказе
    amountJob = 0;
    for (var i = 0; i < jobs.length; i++) {
        amountJob += +jobs[i].amountServiceJob;
    }
    $('#orderPrice').val(amountJob);
    $("#total").val(amountJob);
}

function editOrder() {
    ordersList(function (data) {
        $("#numberOrderList option").remove();
        $("#numberOrderList").append($('<option value="" disabled></option>'));
        for (var i = 0; i < data.length; i++) {
            $("#numberOrderList").append($('<option value="' + data[i].numberOrder + '">' + data[i].numberOrder + '</option>'));
        }
    });
}
//функция начала выполнения работ в модальном окне "изменить заказ"
function startWork() {
    var date = dateNormal.dateFormat();
    $("#changeDateBeginOrder").val(date);

    var time = timeNormal.timeFormat();
    $("#changeTimeBeginOrder").val(time);
}

//функция окончания выполнения работ в модальном окне "изменить заказ"
function finishWork() {
    var date = dateNormal.dateFormat();
    $("#changeDateEndOrder").val(date);

    var time = timeNormal.timeFormat();
    $("#changeTimeEndOrder").val(time);
}

var dataServices = new Array();//массив для записи данных о работе полученных в следующей функции
//отображение списка работ заказа для изменения данных о работе в модальном окне изменения заказа
function changeOrderListServices() {
    var numberOrder = $('#numberOrderList').val();
    workOrderList(numberOrder, function (data) {
        $("#changeWorkNameService option").remove();
        $("#changeWorkNameService").append($('<option value=""></option>'));
        for (var i = 0; i < data.length; i++) {
            $("#changeWorkNameService").append($('<option value="' + data[i].name + '">' + data[i].name + '</option>'));
        }
        dataServices = data;
    });
    //отображает список доступных механиков
    mechanicList(function (data) {
        $("#changeWorkExecutorService option").remove();
        $("#changeWorkExecutorService").append($('<option value=""></option>'));
        for (var i = 0; i < data.length; i++) {
            $("#changeWorkExecutorService").append($('<option value="' + data[i].id + '">' + data[i].login + '</option>'));
        }
    })
}
//отображение данных о работе при изменении информации
function changeWorkInform(nameService) {
    for (var i = 0; i < dataServices.length; i++) {
        if (dataServices[i].name = nameService.value) {
            $("#changeWorkServiceCode").val(dataServices[i].idServices);
            $("#changeWorkPriceService").val(dataServices[i].price);
            $("#changeWorkQuantityService").val(dataServices[i].quantity);
            $("#changeWorkAmountService").val(dataServices[i].amount);
            $("#changeWorkDateBeginService").val(dataServices[i].dateBegin);
            $("#changeWorkDateEndService").val(dataServices[i].endDate);
            $("#changeWorkStatusService").val(dataServices[i].status);
            $("#changeWorkExecutorService").val(dataServices[i].login);
            $("#changeWorkResponsibleService").val(dataServices[i].responsible);
            $("#changeWorkNotesService").val(dataServices[i].notes);
            $("#changeWorkCodeClient").val(dataServices[i].idClient);
            $("#changeWorkCodeOrder").val(dataServices[i].codeOrder);
            $("#changeWorkId").val(dataServices[i].id);
        }
    }
}

//функция внесения изменений в выбранную работу в форме изменение заказа
function changeWork() {
    var form = $('#changeWorkForm').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    for (var i = 0; i <dataServices.length; i++) {
        if (result.changeWorkId =  dataServices[i].id) {
            dataServices[i].quantity = result.changeWorkQuantityService;
            dataServices[i].amount = result.changeWorkAmountService;
            dataServices[i].dateBegin = result.changeWorkDateBeginService;
            dataServices[i].endDate = result.changeWorkDateEndService;
            dataServices[i].status = result.changeWorkStatusService;
            dataServices[i].executor = result.changeWorkExecutorService;
            dataServices[i].notes = result.changeWorkNotesService;
        }
    }
    //отображение внесенных изменений в таблицу отображающую предоставленные работы в данном заказе
    $("#tableChangeWorksOrder tbody").remove();
    for (var i = 0; i < dataServices.length; i++) {
        $('#tableChangeWorksOrder').append("<tr>" +
            '<td>' + (i + 1) + '</td>' +
            '<td>' + dataServices[i].idServices + '</td>' +
            '<td>' + dataServices[i].name + '</td>' +
            '<td>' + dataServices[i].quantity + '</td>' +
            '<td>' + dataServices[i].price + '</td>' +
            '<td>' + dataServices[i].amount + '</td>' +
            '<td>' + dataServices[i].dateBegin + '</td>' +
            '<td>' + dataServices[i].endDate + '</td>' +
            '<td>' + dataServices[i].responsible + '</td>' +
            '<td>' + dataServices[i].login + '</td>' +
            '<td>' + dataServices[i].status + '</td>' +
            '<td>' + dataServices[i].codeOrder + '</td>' +
            '</tr>');
    }


}


$("#addOrderButton").click(function () {
    $("#datetimeorder").val(Date());
});

$(document).ready(function () {
    $("#addClientModal").validationEngine();
    $("#addOrderModal").validationEngine();
    $("#changeOrderModal").validationEngine();
    $("#ChangeClientModal").validationEngine();
    $("#RequestSQLModal").validationEngine();
    $("#WordTableExportModal").validationEngine();
    $("#ExelTableExportModal").validationEngine();
    $("#HTMLTableExportModal").validationEngine();
});


//получение даты и времени в читаемом виде
var dateNormal = {
    date : new Date(),
    format : new Intl.DateTimeFormat("ru"),
    dateFormat: function () {
        return this.format.format(this.date);
    }
};

var timeNormal = {
    time: new Date(),
    format: new Intl.DateTimeFormat("ru", {
        hour: "numeric",
        minute: "numeric"
    }),
    timeFormat: function () {
        return this.format.format(this.time);
    }
};
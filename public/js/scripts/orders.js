var nameJob;
var nameSparePart;
function newOrder() {
    //создание номера заказа
    var random = Math.random() * (999999999999 - 1) +1;
    var numberOrder = "АД" + random;
    numberOrder = numberOrder.toString().replace(".","-");
    $("#numberOrder").val(numberOrder);
    $("#codeOrderJob").val(numberOrder);
    $("#codeOrderSparePart").val(numberOrder);
    $("#codeOrderPayment").val(numberOrder);
    $("#codeOrderProduct").val(numberOrder);

    //получение даты и времени при создании нового заказа
    var date = new Date();
    var format = new Intl.DateTimeFormat("ru");
    date = format.format(date);
    $("#dateReceipt").val(date);
    $("#dateBeginJob").val(date);
    $("#datePayment").val(date);
    $("#dateSaleProduct").val(date);

    var time = new Date();
    format = new Intl.DateTimeFormat("ru", {
        hour: "numeric",
        minute: "numeric"
    });
    time = format.format(time);
    $("#timeReceipt").val(time);

    $(".chosen-select").chosen();
}

//вывод id слиента
function idclient(id) {
    $("#idClient").val(id.value);
    $("#codeClientJob").val(id.value);
    $("#codeClientSparePart").val(id.value);
    $("#codeClientPayment").val(id.value);
    $("#codeClientProduct").val(id.value);
}

//отправка ajax-запроса на получение данных об услуге вывод полученных значений
function ajaxServiceId(id) {
    $.get('/services/id/'+id, function (data) {
        data = JSON.parse(data);
        $("#serviceCodeJob").val(data.id);
        $("#priceServiceJob").val(data.price);
        nameJob = data.name;
    });
}

//ajax-запрос на получени информации о выбранной запчасти и вывод полученных значений
function ajaxSparePart(id) {
    $.get('/spareparts/id/'+id, function (data) {
        data = JSON.parse(data);
        $("#codeSparePart").val(data.id);
        $("#unitSparePart").val(data.unit);
        $("#vendorCodeSparePart").val(data.vendorCode);
        $("#priceSparePart").val(data.sellingPrice);
        nameSparePart = data.name;
    })

}
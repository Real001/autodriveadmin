function informOrder(numberOrder, callback) {
    $.get('/order/information/'+numberOrder.value, function (data) {
        data = JSON.parse(data);
        console.log(data);
        $("#changeDateReceiptOrder").val(data[0].dateReceipt);
        $("#changeTimeReceiptOrder").val(data[0].timeReceipt);
        $("#changeDateBeginOrder").val(data[0].dateBegin);
        $("#changeTimeBeginOrder").val(data[0].timeBegin);
        $("#changeOrderPrice").val(data[0].orderPrice);
        $("#changeCostSpareParts").val(data[0].costSpareParts);
        $("#changeTotalOrder").val(data[0].total);
        $("#changeDateEndOrder").val(data[0].endDate);
        $("#changeTimeEndOrder").val(data[0].endTime);
        $("#changeEmployeeOrder").val(data[0].idEmployee);
        $("#changeStatusOrder").val(data[0].status);
        $("#changeIdClientOrder").val(data[0].idClient);
    });

    workOrderList(numberOrder.value, function (data) {
        $("#tableChangeWorksOrder tbody").remove();
        for (var i = 0; i < data.length; i++) {
            $('#tableChangeWorksOrder').append("<tr>" +
                '<td>' + (i + 1) + '</td>' +
                '<td>' + data[i].idServices + '</td>' +
                '<td>' + data[i].name + '</td>' +
                '<td>' + data[i].quantity + '</td>' +
                '<td>' + data[i].price + '</td>' +
                '<td>' + data[i].amount + '</td>' +
                '<td>' + data[i].dateBegin + '</td>' +
                '<td>' + data[i].endDate + '</td>' +
                '<td>' + data[i].responsible + '</td>' +
                '<td>' + data[i].login + '</td>' +
                '<td>' + data[i].status + '</td>' +
                '<td>' + data[i].codeOrder + '</td>' +
                '</tr>');
        }
    })
}
function addjob() {
    var form = $('#addOrderForm').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    sendJSON('/addOrder', result, function (err, data) {
        if (!err && data.status == "success") {
            window.location.href = "/orders";
        }
    });
}
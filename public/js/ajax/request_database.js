//вывопление любого запроса
function sqlRequest() {
    var form = $('#sqlRequestText').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    sendJSON('/requestDatabase',result , function (err, data) {
        if (!err) {
            data = JSON.stringify(data);
            $('#responseSql').val(data);
        }
    });
}
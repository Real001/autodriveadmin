function addOrder(id) {
    var form = $('#addOrderForm').serializeArray();
    var result = new Object();

    //получаем данные с формы нового заказа и сохраняем их в переменную result
    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }

    result['employee'] = id;

    // вызов post запроса на сохраение массива объектов jobs (работы) в БД
    for (var i =0; i < jobs.length; i++) {
        sendJSON('/addAllWorks', jobs[i], function (err, data) {
            if (!err && data.status == "success") {
            }
        });
    }
    if (sparePart) { //проверка, если есть запчасти выполняется
        for (var i = 0; i < sparePart.length; i++) {
            sendJSON('/addSparePartsOperation', sparePart[i], function (err, data) {

            });
        }
    }

    sendJSON('/addOrder', result, function (err, data) {
        if (!err && data.status == "success") {
            if (sparePart) {
                window.location.href = "/orders";
            } else {
                window.location.href = "/clients"
            }
        }
    });
}

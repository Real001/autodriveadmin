
function createSpareParts() {
    //получение данных формы SpareParts
    var form = $('#addSpareParts').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
    }
    //отправка данных через ajax, для последующей их обработки
    sendJSON('/addSpareParts', result, function (err, data) {
        if (!err && data.status == "success") {
            window.location.href = "/spareparts";
        }
    });
}

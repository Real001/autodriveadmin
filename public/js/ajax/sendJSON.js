function sendJSON(url, obj, callback) {
    $.ajax({
        url: url,
        method: "POST",
        contentType: "application/json;charset=utf8",
        dataType: "JSON",
        data: JSON.stringify(obj),
        success: function (data) {
            callback(null, data);
        },
        error: function (jqXHR, textStatus) {
            callback(textStatus);
        }
    });
}


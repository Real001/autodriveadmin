
function addClient() {
    var form = $('#addClient').serializeArray();
    var result = new Object();

    for (var i = 0; i < form.length; i++) {
        result[form[i]['name']] = form[i]['value'];
        //console.log(result[form[i]['name']]);
    }

    sendJSON('/addClient', result, function (err, data) {
        if (!err && data.status == "success") {
            window.location.href = "/clients";
        }
    });
}

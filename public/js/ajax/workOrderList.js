//получаем список работ указанного заказа  по номеру заказа
function workOrderList(numberOrder, callback) {
    $.get('/order/services/' + numberOrder, function (data) {
        data = JSON.parse(data)
        callback(data);
    });
}
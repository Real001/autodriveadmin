var model = require('../models/employee');
var controller = new Object();

controller.employee = function () {
    this.getAllEmployee = function (req, res) {
        var employee = model.employee;
        employee.getAllEmployee(function (err, employee) {
            if (!err) {
                if (!employee)
                {
                    employee = 0;
                }
                res.render('employees/employees', {employee: employee, user: req.user});
            }
        });
    }

    this.getAllEmployeeAutomechanic = function (req, res) {
        var employee = model.employee;
        employee.getAllEmployeeAutomechanic(function (err, employee) {
            if (!err) {
                if (!employee)
                {
                    employee = 0;
                }
                console.log(req.params.rend)
                if (req.params.rend){
                    res.end(JSON.stringify(employee));
                } else {
                    res.render('employees/mechanic', {employee: employee, user: req.user});
                }
            }
        });
    }

    this.getAllEmployeeInspector = function (req, res) {
        var employee = model.employee;
        employee.getAllEmployeeInspector(function (err, employee) {
            if (!err) {
                if (!employee)
                {
                    employee = 0;
                }
                res.render('employees/inspectors', {employee: employee, user: req.user});
            }
        });
    }

    this.getAllEmployeeSeller = function (req, res) {
        var employee = model.employee;
        employee.getAllEmployeeSeller(function (err, employee) {
            if (!err) {
                if (!employee)
                {
                    employee = 0;
                }
                res.render('employees/sellers', {employee: employee, user: req.user});
            }
        });
    }
}

module.exports = controller;
var model = require('../models/services');
var controller = new Object() ;

controller.services = function () {
    this.getServices = function (req, res) {
        var services = model.services;
        services.getServices(req.params.id, function (err, services) {
            if (!err) {
                if (!services) {
                    services = 0;
                }
                res.end(JSON.stringify(services));
            }
        });
    }

    this.getAllServices = function (req, res) {
        var services = model.services;
        services.getAllServices(function (err, services) {
            if (!err) {
                if (!services) {
                    services = 0;
                }
                res.render('services', {services: services, user: req.user});
            }
        });
    }

    this.addService = function (req, res) {
        var name = req.body.name;
        var category = req.body.category;
        var price = req.body.price;
        var relatedMaterials = req.body.relatedMaterials;
        var notes = req.body.notes;
        var typeCar = req.body.typeCar;

        var services = model.services;
        services.addService({ name: name, category: category, price: price, relatedMaterials: relatedMaterials, notes: notes, typeCar: typeCar},
            function (err) {
            if (!err){
                res.end(JSON.stringify({status: "success"}));
            }
        });

    }

    this.getServicesList = function (req, res) {
        var services = model.services;
        services.getAllServices(function (err, services) {
            if (!err) {
                if (!services) {
                    services = 0;
                }
                res.end(JSON.stringify(services));
            }
        });
    }
}

module.exports = controller;
var model = require('../models/sparePartsOperation');
var controller = new Object();

controller.sparePartsOperation = function () {
    //добавление запчастей используемых в заказе
    this.addSparePartsOperation = function (req, res) {
        var idSpareParts = req.body.codeSparePart;
        var spareParts = req.body.nameSparePart;
        var unit = req.body.unitSparePart;
        var vendorCode = req.body.vendorCodeSparePart;
        var price = req.body.priceSparePart;
        var quantity = req.body.quantitySparePart;
        var amount = req.body.amountSparePart;
        var idClient = req.body.codeClientSparePart;
        var codeOrder = req.body.codeOrderSparePart;

        var sparePartsOperation = model.sparePartsOperation;
        sparePartsOperation.addSparePartsOperation({idSpareParts: idSpareParts, spareParts: spareParts, unit: unit, vendorCode: vendorCode,
        price: price, quantity: quantity, amount: amount, idClient: idClient, codeOrder: codeOrder}, function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    }
};

module.exports = controller;

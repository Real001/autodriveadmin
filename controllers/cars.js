var model = require('../models/cars');
var controller = new Object();

controller.cars = function () {

    this.addCars = function (req, res) {
        var type = req.body.typeCar;
        var brand = req.body.brandCar;
        var year = req.body.yearCar;
        var stateNumber = req.body.stateNumberCar;
        var VIN = req.body.VINCar;
        var modelCar = req.body.modelCar;
        var idClient = req.body.idClient;
        var mileage = req.body.mileageCar;

        var cars = model.cars;
        cars.addCars({ type: type, brand: brand, model: modelCar, year: year, stateNumber: stateNumber, VIN: VIN, mileage: mileage, idClient: idClient},
            function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    }

}
module.exports = controller;
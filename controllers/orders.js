var modelOrder = require('../models/orders');
var modelClient = require('../models/client');
var modelService = require('../models/services');
var modelEmployee = require('../models/employee');
var modelSparePart = require('../models/spareParts');
var controller = new Object();


controller.orders = function () {

   // используется вложенные запросы
    this.getAllOrders = function (req, res) {
        var orders = modelOrder.orders;
        //получение всех элементов orders
        orders.getAllOrders(function (err, orders) {
            if (!err) {
                if (!orders) {
                    orders = 0;
                }
                var clients = modelClient.clients;
                clients.getAllClients(function (err, clients) {
                    if (!err) {
                        if (!clients) {
                            clients = 0;
                        }
                        var services = modelService.services;
                        services.getAllServices(function (err, services) {
                            if (!err) {
                                if (!services) {
                                    services = 0;
                                }
                                var employee = modelEmployee.employee;
                                employee.getAllEmployeeAutomechanic(function (err, employee) {
                                    if (!err) {
                                        if (!employee) {
                                            employee = 0;
                                        }
                                        var sparePart = modelSparePart.spareParts;
                                        sparePart.getAllSpareParts(function (err, spareParts) {
                                            if (!err) {
                                                if (!spareParts) {
                                                    spareParts = 0;
                                                }
                                                res.render('orders', {
                                                    orders: orders,
                                                    clients: clients,
                                                    services: services,
                                                    employee: employee,
                                                    spareParts: spareParts,
                                                    user: req.user
                                                });
                                            }
                                        });
                                    }
                                });
                            }

                        })
                    }
                });
            }
        });

    }

//добавление нового заказа в БД
    this.addOrders = function (req, res) {
        var numberOrder = req.body.numberOrder;
        var idClient = req.body.idClient;
        var idEmployee = req.body.employee;
        var orderPrice = req.body.orderPrice;
        var costSpareParts = req.body.costSpareParts;
        var total = req.body.total;
        var status = req.body.status;
        var dateReceipt = req.body.dateReceipt;
        var timeReceipt = req.body.timeReceipt;
        var dateBegin = req.body.dateBegin;
        var endDate = req.body.endDate;
        var timeBegin = req.body.timeBegin;
        var endTime = req.body.endTime;

        var orders = modelOrder.orders;
        orders.addOrders({ numberOrder: numberOrder, idClient: idClient, idEmployee: idEmployee, orderPrice: orderPrice, costSpareParts: costSpareParts, total: total,
            status: status, dateReceipt: dateReceipt, timeReceipt: timeReceipt, dateBegin: dateBegin, endDate: endDate, timeBegin: timeBegin, endTime: endTime}, function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    };

    this.getAllOrdersClient = function (req, res) {
        var orders = modelOrder.orders;
        orders.getAllOrdersClient(req.params.id, function (err, orders) {
            if (!err) {
                if (!orders) {
                    orders = 0;
                }
                res.end(JSON.stringify(orders));
            }

        });
    };

    this.getOrdersList = function (req, res) {
        var orders = modelOrder.orders;
        orders.getAllOrders(function (err, orders) {
            if (!err) {
                if (!orders) {
                    orders = 0;
                }
                res.end(JSON.stringify(orders));
            }
        });
    };
    
    this.getOrder = function (req, res) {
        var orders = modelOrder.orders;
        var numberOrder = decodeURIComponent(req.params.numberOrder);
        orders.getOrder(numberOrder, function (err, order) {
            if (!err) {
                if (!order) {
                    order = 0;
                }
                res.end(JSON.stringify(order));
            }
        });
    }

    this.updateOrder = function (req, res) {
        var numberOrder = req.body.numberOrderList;
        var orderPrice = req.body.changeOrderPrice;
        var costSpareParts = req.body.changeCostSpareParts;
        var total = req.body.changeTotalOrder;
        var status = req.body.changeStatusOrder;
        var dateBegin = req.body.changeDateBeginOrder;
        var endDate = req.body.changeDateEndOrder;
        var timeBegin = req.body.changeTimeBeginOrder;
        var endTime = req.body.changeTimeEndOrder;

        var orders = modelOrder.orders;
        orders.updateOrder({ numberOrder: numberOrder, orderPrice: orderPrice, costSpareParts: costSpareParts, total: total, status: status,
             dateBegin: dateBegin, endDate: endDate, timeBegin: timeBegin, endTime: endTime},
            function (err) {
                if (!err) {
                    res.end(JSON.stringify({status: "success"}));
                }
        });
    }
};

module.exports = controller;
var model = require('../models/spareParts');
var controller = new Object() ;

controller.spareParts = function () {
    this.getSpareParts = function (req, res) {
        var spareParts = model.spareParts;
        spareParts.getSpareParts(req.params.id, function (err, spareParts) {
            if (!err) {
                if (!spareParts) {
                    spareParts = 0;
                }
                res.end(JSON.stringify(spareParts));
            }
        });
    }

    this.getAllSpareParts = function (req, res) {
        var spareParts = model.spareParts;
        spareParts.getAllSpareParts(function (err, spareParts) {
            if (!err) {
                if (!spareParts) {
                    spareParts = 0;
                }
                res.render('spareparts', {spareParts: spareParts, user: req.user});
            }
        });
    }

    this.addSpareParts = function (req, res) {
        //присвоение переменным полученых значений от клиента для добавления в бд новой запчасти
        var name = req.body.name;
        var vendorCode = req.body.vendorCode;
        var manufacturer = req.body.manufacturer;
        var manufacturerCountry = req.body.manufacturerCountry;
        var application = req.body.application;
        var OEM = req.body.OEM;
        var price = req.body.price;
        var sellingPrice = req.body.sellingPrice;
        var unit = req.body.unit;
        var notes = req.body.notes;

        var spareParts = model.spareParts;
        spareParts.addSpareParts({ name: name, vendorCode: vendorCode, manufacturer: manufacturer, manufacturerCountry: manufacturerCountry,
            application: application, OEM: OEM, price: price, sellingPrice: sellingPrice, unit: unit, notes: notes},
            function (err) {
                if (!err){
                    res.end(JSON.stringify({status: "success"}));
                }
            });

    }
}

module.exports = controller;
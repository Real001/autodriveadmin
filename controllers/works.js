var model = require('../models/works');

var controller = new Object();

controller.works = function () {

    this.addAllWorks = function (req, res) {

        var idServices = req.body.serviceCodeJob;
        var services = req.body.serviceJob;
        var price = req.body.priceServiceJob;
        var quantity = req.body.quantityServiceJob;
        var amount = req.body.amountServiceJob;
        var dateBegin = req.body.dateBeginJob;
        if (req.body.dateExpJob != '') {
            var endDate = req.body.dateExpJob;
        }
        var status = req.body.statusJob;
        var notes = req.body.notesJob;
        var executor = req.body.executorJob;
        var responsible = req.body.responsibleJob;
        var idClient = req.body.codeClientJob;
        var codeOrder = req.body.codeOrderJob;
        var idCar = req.body.idCar;

        var works = model.works;
        works.addAllWorks({ idServices: idServices, services: services, price: price, idClient: idClient, quantity: quantity, amount: amount, codeOrder: codeOrder,
            status: status, notes: notes, executor: executor, dateBegin: dateBegin, endDate: endDate, responsible: responsible, idCar: idCar}, function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    }
//вывод списка работ заказа
    this.getAllWorksOrder = function (req, res) {
        var works = model.works;
        var numberOrder = decodeURI(req.params.numberOrder);
        works.getAllWorksOrder(numberOrder, function (err, works) {
            if (!err) {
                if (!works) {
                    works = 0;
                }
                res.end(JSON.stringify(works));
            }
        });
    }
}

module.exports = controller;
var model = require('../models/client');
var controller = new Object();

controller.clients = function () {

    this.getClients = function (req, res) {
        var clients = model.clients;
        clients.getClients(req.params.id, function (err, client) {
            if (!err) {
                if (!client) {
                    client = 0;
                }
                console.log(client);
                res.end(JSON.stringify(client));
            }

        });
    };

    this.getAllClients = function (req, res) {
       var clients = model.clients;
        clients.getAllClients(function(err, clients) {
            if (!err) {
                res.render('clients', {clients: clients, user: req.user});
            }
        });
    };

    this.addClients = function (req, res) {
        var fullName = req.body.fullName;
        var typeClient = req.body.typeClient;
        var phone = req.body.phone;
        var email = req.body.email;
        var typeCar = req.body.typeCar;
        var brand = req.body.brand;
        var modelCar = req.body.model;
        var year = req.body.year;
        var stateNumber = req.body.stateNumber;
        var VIN = req.body.VIN;
        var mileage = req.body.mileage;

        var clients = model.clients;
        clients.addClients({ fullName: fullName, typeClient: typeClient, phone: phone, email: email, type: typeCar, brand: brand,
        model: modelCar, year: year, stateNumber: stateNumber, VIN: VIN, mileage: mileage}, function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    }

    this.updateClient = function (req, res) {
        var id = req.body.idCI
        var fullName = req.body.fullNameCI;
        var typeClient = req.body.typeClientCI;
        var phone = req.body.phoneCI;
        var email = req.body.emailCI;

        var clients = model.clients;
        clients.updateClient({id: id, fullName: fullName, typeClient: typeClient, phone: phone, email: email}, function (err) {
            if (!err) {
                res.end(JSON.stringify({status: "success"}));
            }
        });
    }

};

module.exports = controller;
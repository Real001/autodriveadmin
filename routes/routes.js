// app/routes.js
var clientControllers = require('../controllers/client');
var clients = new clientControllers.clients();
var serviceControllers = require('../controllers/services');
var service = new serviceControllers.services();
var orderControllers = require('../controllers/orders');
var orders = new orderControllers.orders();
var employeeControllers = require('../controllers/employee');
var employee = new employeeControllers.employee();
var sparePartsControllers = require('../controllers/spareParts');
var spareParts = new sparePartsControllers.spareParts();
var worksController = require('../controllers/works');
var works = new worksController.works();
var sparePartsOperationController = require('../controllers/sparePartsOperation');
var sparePartsOperation = new sparePartsOperationController.sparePartsOperation();
var carsController = require('../controllers/cars');
var cars = new carsController.cars();
var requestController = require('../controllers/request');
var request1 = new requestController.requestDatabase();


module.exports = function(app, passport) {


	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('index'); // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// process the login form
	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/start', // redirect to the secure profile section
            failureRedirect : '/', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/start', // redirect to the secure profile section
		failureRedirect : '/', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/start', isLoggedIn, function(req, res) {
		res.render('start', {
			user : req.user // get the user out of session and pass to template
		});
	});

	app.get('/clients', isLoggedIn, clients.getAllClients);
	app.post('/addClient',isLoggedIn, clients.addClients);
	app.get('/client/id/:id', isLoggedIn, clients.getClients);
	app.post('/updateClient', isLoggedIn, clients.updateClient);
	app.get('/client/orders/:id', isLoggedIn, orders.getAllOrdersClient);

    app.get('/services', isLoggedIn, service.getAllServices);
    app.post('/addService', isLoggedIn, service.addService);
    app.get('/services/id/:id', isLoggedIn, service.getServices);
    app.get('/services/list', isLoggedIn, service.getServicesList);

    app.get('/spareparts', isLoggedIn, spareParts.getAllSpareParts);
    app.post('/addSpareParts', isLoggedIn, spareParts.addSpareParts);
    app.get('/spareparts/id/:id', isLoggedIn, spareParts.getSpareParts);

    app.post('/addSparePartsOperation', isLoggedIn, sparePartsOperation.addSparePartsOperation);

    app.get('/orders', isLoggedIn, orders.getAllOrders);
    app.post('/addOrder', isLoggedIn, orders.addOrders);
    app.get('/order/services/:numberOrder', isLoggedIn, works.getAllWorksOrder);
    app.get('/orders/list', isLoggedIn, orders.getOrdersList);
    app.get('/order/information/:numberOrder', isLoggedIn, orders.getOrder);
    app.post('/order/update', isLoggedIn, orders.updateOrder);

    app.get('/schedule', isLoggedIn, function (req, res) {
		res.render('schedule', {user:req.user});
    });

    app.get('/employees', isLoggedIn, employee.getAllEmployee);
    app.get('/employees/mechanic(/:rend)?', isLoggedIn, employee.getAllEmployeeAutomechanic);
    app.get('/employees/inspectors', isLoggedIn, employee.getAllEmployeeInspector);
    app.get('/employees/sellers', isLoggedIn, employee.getAllEmployeeSeller);

    app.get('/admission', isLoggedIn, function (req, res) {
		res.render('admission', {user: req.user});
    });

    app.get('/selling', isLoggedIn, function (req, res) {
		res.render('selling', {user: req.user});
    });

    app.get('/write-off', isLoggedIn, function (req, res) {
		res.render('write-off', {user: req.user});
    });

    app.get('/warehouse_condition', isLoggedIn, function (req, res) {
		res.render('warehouse_condition', {user: req.user});
    });

    app.get('/contracts', isLoggedIn, function (req,res) {
		res.render('contracts', {user: req.user});
    });

    app.get('/warehouses', isLoggedIn, function (req, res) {
        res.render('warehouses', {user: req.user});
    });

    app.get('/suppliers', isLoggedIn, function (req, res) {
        res.render('suppliers', {user: req.user});
    });

    app.get('/work', isLoggedIn, function (req, res) {
        res.render('work', {user: req.user});
    });
    app.post('/addAllWorks', isLoggedIn, works.addAllWorks);
    app.get('/works/order/:numberOrder', isLoggedIn, works.getAllWorksOrder);

    app.get('/payments', isLoggedIn, function (req, res) {
        res.render('payments', {user: req.user});
    });

    app.get('/sold_goods', isLoggedIn, function (req, res) {
        res.render('sold_goods', {user: req.user});
    });

    app.get('/arrived_goods', isLoggedIn, function (req, res) {
        res.render('arrived_goods', {user: req.user});
    });

    app.get('/written-off_goods', isLoggedIn, function (req, res) {
        res.render('written-off_goods', {user: req.user});
    });

    app.get('/chronology', isLoggedIn, function (req, res) {
        res.render('chronology', {user: req.user});
    });

    app.get('/history_of_changes', isLoggedIn, function (req, res) {
        res.render('history_of_changes', {user: req.user});
    });

    app.post('/addCarClient', isLoggedIn, cars.addCars);

    app.post('/requestDatabase', isLoggedIn, request1.getRequestDatabase);
	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}
